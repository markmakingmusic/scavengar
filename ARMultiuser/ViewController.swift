/*
See LICENSE folder for this sample’s licensing information.

Abstract:
Main view controller for the AR experience.
*/

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {
    // MARK: - IBOutlets
    
    @IBOutlet weak var sessionInfoView: UIView!
    @IBOutlet weak var sessionInfoLabel: UILabel!
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var placeNextClueButton: UIButton!
    @IBOutlet weak var mappingStatusLabel: UILabel!
    
    private var isScavenging = false
    
    private struct Defaults {
        static let minNumberOfCluesRequired = 3
    }
    private var clues = [(ARWorldMap, String)]()
    
    // MARK: - View Life Cycle
    
    var canPlaceNextClue: Bool {
        return self.currentClueAnchor != nil
    }
    
    var canSendMapsToPeers: Bool {
        return self.clues.count == Defaults.minNumberOfCluesRequired
    }
    
    var currentClueAnchor: ARAnchor? {
        return self.sceneView.session.currentFrame?.anchors
            .filter { ($0.name?.hasPrefix("panda") == true) }
            .first
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard ARWorldTrackingConfiguration.isSupported else {
            fatalError("""
                ARKit is not available on this device. For apps that require ARKit
                for core functionality, use the `arkit` key in the key in the
                `UIRequiredDeviceCapabilities` section of the Info.plist to prevent
                the app from installing. (If the app can't be installed, this error
                can't be triggered in a production scenario.)
                In apps where AR is an additive feature, use `isSupported` to
                determine whether to show UI for launching AR experiences.
            """) // For details, see https://developer.apple.com/documentation/arkit
        }
        
        // Start the view's AR session.
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration)
        
        // Set a delegate to track the number of plane anchors for providing UI feedback.
        sceneView.session.delegate = self
        
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        // Prevent the screen from being dimmed after a while as users will likely
        // have long periods of interaction without touching the screen or buttons.
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's AR session.
        sceneView.session.pause()
    }
    
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if let name = anchor.name, name.hasPrefix("panda") {
            node.addChildNode(self.loadRedPandaModel())
        }
    }
    
    // MARK: - ARSessionDelegate
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        self.updateSessionInfoLabel(for: session.currentFrame!, trackingState: camera.trackingState)
    }
    
    /// - Tag: CheckMappingStatus
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        self.mappingStatusLabel.text = frame.worldMappingStatus.description
        self.updateSessionInfoLabel(for: frame, trackingState: frame.camera.trackingState)

        guard !self.isScavenging else {
            self.placeNextClueButton.isHidden = true
            return
        }
        switch frame.worldMappingStatus {
        case .notAvailable, .limited:
            self.placeNextClueButton.isHidden = true
        case .extending:
            self.placeNextClueButton.isHidden = true
        case .mapped:
            self.placeNextClueButton.isHidden = !self.canPlaceNextClue
        }
    }
    
    // MARK: - ARSessionObserver
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay.
        sessionInfoLabel.text = "Session was interrupted"
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required.
        sessionInfoLabel.text = "Session interruption ended"
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user.
        sessionInfoLabel.text = "Session failed: \(error.localizedDescription)"
        resetTracking(nil)
    }
    
    func sessionShouldAttemptRelocalization(_ session: ARSession) -> Bool {
        return true
    }
    
    // MARK: - Multiuser shared session
    
    /// - Tag: PlaceCharacter
    @IBAction func handleSceneTap(_ sender: UITapGestureRecognizer) {
        guard !self.isScavenging else {
            let hitTestSKResults = sceneView.hitTest(sender.location(in: sceneView), options: [:])
            print("Hit Test Results:\n\(hitTestSKResults)")
            
            if hitTestSKResults.contains(where: {
                if let anchor = self.sceneView.anchor(for: $0.node), anchor.name?.hasPrefix("panda") == true {
                    return true
                }
                return false
            }) {
                print("Touched the panda, yay!")
                self.placeNextClue(isFirstClue: false)
            }
            return
        }
        
        // Hit test to find a place for a virtual object.
        guard let hitTestARResult = sceneView
            .hitTest(sender.location(in: sceneView), types: [.existingPlaneUsingGeometry, .estimatedHorizontalPlane])
            .first
            else { return }
        
        // Place an anchor for a virtual character. The model appears in renderer(_:didAdd:for:).
        let anchor = ARAnchor(name: "panda", transform: hitTestARResult.worldTransform)
        sceneView.session.add(anchor: anchor)
    }
    
    @IBAction private func placeNextClueButtonTapped(_ button: UIButton) {
        guard !self.isScavenging else {
            return
        }
        
        let alertController = UIAlertController(title: NSLocalizedString("Describe it", comment: ""), message: NSLocalizedString("Help the scavenger find the clue you just placed!", comment: ""), preferredStyle: .alert)
        alertController.addTextField { textField in
            textField.placeholder = NSLocalizedString("Arrrggghhh matey...", comment: "")
            textField.textColor = .darkGray
            textField.clearButtonMode = .whileEditing
            textField.borderStyle = .roundedRect
            textField.autocapitalizationType = .sentences
        }
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Done!", comment: ""), style: .default, handler: { [weak self] _ in
            guard let `self` = self else { return }
            
            self.sceneView.session.getCurrentWorldMap { [weak self] map, error in
                guard let `self` = self else { return }
                
                guard error == nil else {
                    print(String(describing: error?.localizedDescription))
                    return
                }
                guard let map = map else {
                    print("No map available.")
                    return
                }
                let clueText = alertController.textFields?.compactMap { $0.text }.first
                    ?? NSLocalizedString("No clue was provided...You're just gonna have to figure it out for yourself!", comment: "")
                self.clues.append((map, clueText))
                if self.clues.count == Defaults.minNumberOfCluesRequired {
                    self.isScavenging = true
                    self.placeNextClue(isFirstClue: true)
                } else {
                    self.placeNextClueButton.setTitle(NSLocalizedString("Place next clue!", comment: ""), for: .normal)
                    self.isScavenging = false
                    self.resetTracking(nil)
                }
            }
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func placeNextClue(isFirstClue: Bool) {
        guard !self.clues.isEmpty else {
            print("No more clues to consume.")
            let alertController = UIAlertController(title: nil, message: NSLocalizedString("Congratulations!", comment: ""), preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            self.isScavenging = false
            return
        }
        
        let clue = self.clues.removeFirst()
        let message = isFirstClue
            ? NSLocalizedString("Your first clue is:\n\n\(clue.1)", comment: "")
            : NSLocalizedString("You Found a clue! Here's your next one:\n\n\(clue.1)", comment: "")
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { [weak self] _ in
            // Run the session with the received world map.
            let configuration = ARWorldTrackingConfiguration()
            configuration.planeDetection = .horizontal
            configuration.initialWorldMap = clue.0
            self?.sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - AR session management
    
    private func updateSessionInfoLabel(for frame: ARFrame, trackingState: ARCamera.TrackingState) {
        // Update the UI to provide feedback on the state of the AR experience.
        let message: String
        
        switch trackingState {
        case .normal:
            // No planes detected; provide instructions for this app's AR interactions.
            message = "Move around to map the environment, or wait to join a shared session."

        case .notAvailable:
            message = "Tracking unavailable."
            
        case .limited(.excessiveMotion):
            message = "Tracking limited - Move the device more slowly."
            
        case .limited(.insufficientFeatures):
            message = "Tracking limited - Point the device at an area with visible surface detail, or improve lighting conditions."
            
        case .limited(.relocalizing):
            message = "Resuming session — move to where you were when the session was interrupted."
            
        case .limited(.initializing):
            message = "Initializing AR session."
            
        default:
            // No feedback needed when tracking is normal and planes are visible.
            // (Nor when in unreachable limited-tracking states.)
            message = ""
        }
        
        sessionInfoLabel.text = message
        sessionInfoView.isHidden = message.isEmpty
    }
    
    @IBAction func resetTracking(_ sender: UIButton?) {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
    
    // MARK: - AR session management
    private func loadRedPandaModel() -> SCNNode {
        let sceneURL = Bundle.main.url(forResource: "max", withExtension: "scn", subdirectory: "Assets.scnassets")!
        let referenceNode = SCNReferenceNode(url: sceneURL)!
        referenceNode.load()
        
        return referenceNode
    }
}

